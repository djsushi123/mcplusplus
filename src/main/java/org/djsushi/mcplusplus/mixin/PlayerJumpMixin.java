package org.djsushi.mcplusplus.mixin;

import net.minecraft.entity.player.PlayerEntity;
import org.djsushi.mcplusplus.Events;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(PlayerEntity.class)
public abstract class PlayerJumpMixin {

    @Inject(at = @At("TAIL"), method = "jump")
    private void jumpFromDenseCable(CallbackInfo ci) {
        Events.INSTANCE.getBEFORE_PLAYER_JUMP().invoker().onPlayerJump((PlayerEntity) (Object) this);
    }
}