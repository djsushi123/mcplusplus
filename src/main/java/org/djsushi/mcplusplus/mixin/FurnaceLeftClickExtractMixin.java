package org.djsushi.mcplusplus.mixin;

import net.minecraft.block.Blocks;
import net.minecraft.block.entity.FurnaceBlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.c2s.play.PlayerActionC2SPacket;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.network.ServerPlayerInteractionManager;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.stat.Stats;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import org.djsushi.mcplusplus.MCPlusPlus;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ServerPlayerInteractionManager.class)
public abstract class FurnaceLeftClickExtractMixin {


    @Shadow @Final protected ServerPlayerEntity player;

    @Shadow protected ServerWorld world;

    @Inject(
            method = "processBlockBreakingAction",
            at = @At(
                    value = "FIELD",
                    target = "Lnet/minecraft/server/network/ServerPlayerInteractionManager;startMiningTime:I",
                    ordinal = 0,
                    shift = At.Shift.AFTER
            )
    )
    void addFurnaceLeftClickExtract(BlockPos pos, PlayerActionC2SPacket.Action action, Direction direction, int worldHeight, int sequence, CallbackInfo ci) {
        if (world.getBlockState(pos).getBlock() != Blocks.FURNACE) return;
        FurnaceBlockEntity furnaceBE = (FurnaceBlockEntity) world.getBlockEntity(pos);
        if (furnaceBE == null) return; // shouldn't happen but just in case :)
        if (!player.getMainHandStack().isEmpty()) return;
        furnaceBE.dropExperienceForRecipesUsed(player);
        ItemStack stack = furnaceBE.removeStack(2);
        player.increaseStat(Stats.CRAFTED.getOrCreateStat(stack.getItem()), stack.getCount());
        stack.getItem().onCraft(stack, world, player);
        player.getInventory().insertStack(stack);
    }

}