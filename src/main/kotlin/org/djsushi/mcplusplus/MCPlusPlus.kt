package org.djsushi.mcplusplus

import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.minecraft.block.Block
import net.minecraft.block.Material
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemGroup
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import org.djsushi.mcplusplus.block.CanvasBlock
import org.djsushi.mcplusplus.item.PaintBrushItem
import org.slf4j.LoggerFactory

@Suppress("unused")
object MCPlusPlus : ModInitializer {

    const val MODID = "mcplusplus"

    // Blocks
    val CANVAS_BLOCK = CanvasBlock(FabricBlockSettings.of(Material.WOOL))

    // Items
    val PAINT_BRUSH = PaintBrushItem(FabricItemSettings().group(ItemGroup.TOOLS))

    override fun onInitialize() {
        registerBlocks()
    }

    private fun registerBlocks() {
        Registry.register(Registry.BLOCK, Identifier(MODID, "canvas"), CANVAS_BLOCK)
        Registry.register(
            Registry.ITEM,
            Identifier(MODID, "canvas"),
            BlockItem(CANVAS_BLOCK, FabricItemSettings().group(ItemGroup.BUILDING_BLOCKS))
        )
    }

    private fun registerItem() {
        Registry.register(Registry.ITEM, Identifier(MODID, "paint_brush"), PAINT_BRUSH)
    }
}