package org.djsushi.mcplusplus

import appeng.block.networking.CableBusBlock
import appeng.parts.networking.CablePart
import appeng.parts.networking.GlassCablePart
import net.minecraft.block.Block
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.util.math.BlockPos
import kotlin.math.floor


fun PlayerEntity.isStandingOnCable(): Boolean {
    val x = floor(x).toInt()
    val y = y.toInt()
    val z = floor(z).toInt()

    val block: Block = world.getBlockState(BlockPos(x, y, z)).block
    if (block !is CableBusBlock) return false

    val entity = block.getBlockEntity(world, x, y, z)
    val part = entity?.cableBus?.getPart(null)
    
    return part is CablePart && part !is GlassCablePart
}