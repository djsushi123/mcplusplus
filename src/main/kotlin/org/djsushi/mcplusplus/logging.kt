package org.djsushi.mcplusplus

import org.slf4j.LoggerFactory

val logger = LoggerFactory.getLogger(MCPlusPlus.MODID)