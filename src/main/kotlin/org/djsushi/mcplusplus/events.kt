package org.djsushi.mcplusplus

import net.fabricmc.fabric.api.event.Event
import net.fabricmc.fabric.api.event.EventFactory
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.util.ActionResult

object Events {

    val BEFORE_PLAYER_JUMP: Event<PlayerJump> = EventFactory.createArrayBacked(PlayerJump::class.java) { callbacks ->
        PlayerJump { player ->
            for (callback in callbacks) {
                val result = callback.onPlayerJump(player)

                if (result != ActionResult.PASS) return@PlayerJump result
            }

            return@PlayerJump ActionResult.PASS
        }
    }
}

fun interface PlayerJump {
    fun onPlayerJump(player: PlayerEntity): ActionResult
}
