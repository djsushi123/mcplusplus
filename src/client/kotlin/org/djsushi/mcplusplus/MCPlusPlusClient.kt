package org.djsushi.mcplusplus

import net.fabricmc.api.ClientModInitializer
import net.minecraft.client.network.ClientPlayerEntity
import net.minecraft.text.Text
import net.minecraft.util.ActionResult

@Suppress("unused")
object MCPlusPlusClient : ClientModInitializer {
    override fun onInitializeClient() {
        Events.BEFORE_PLAYER_JUMP.register { player ->
            if (player is ClientPlayerEntity && player.input.jumping && player.isStandingOnCable()) {
                player.addVelocity(0.0, 0.011, 0.0) // this is the perfect number to jump exactly
            }
            ActionResult.PASS
        }
    }
}