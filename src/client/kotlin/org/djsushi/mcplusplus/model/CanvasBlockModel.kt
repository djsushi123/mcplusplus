package org.djsushi.mcplusplus.model

import com.mojang.datafixers.util.Pair
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.Material
import net.minecraft.client.render.model.*
import net.minecraft.client.render.model.json.ModelOverrideList
import net.minecraft.client.render.model.json.ModelTransformation
import net.minecraft.client.texture.Sprite
import net.minecraft.client.util.SpriteIdentifier
import net.minecraft.item.ItemStack
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.util.math.random.Random
import net.minecraft.world.BlockRenderView
import java.util.function.Function
import java.util.function.Supplier

class CanvasBlockModel : UnbakedModel, BakedModel, FabricBakedModel {
    override fun getModelDependencies(): MutableCollection<Identifier> = mutableListOf()

    override fun getTextureDependencies(
        unbakedModelGetter: Function<Identifier, UnbakedModel>?,
        unresolvedTextureReferences: MutableSet<Pair<String, String>>?
    ): MutableCollection<SpriteIdentifier> {
        TODO("Not yet implemented")
    }

    override fun bake(
        loader: ModelLoader?,
        textureGetter: Function<SpriteIdentifier, Sprite>?,
        rotationContainer: ModelBakeSettings?,
        modelId: Identifier?
    ): BakedModel? {
        TODO("Not yet implemented")
    }

    override fun getQuads(state: BlockState?, face: Direction?, random: Random?): MutableList<BakedQuad> {
        TODO("Not yet implemented")
    }

    override fun useAmbientOcclusion(): Boolean {
        TODO("Not yet implemented")
    }

    override fun hasDepth(): Boolean {
        TODO("Not yet implemented")
    }

    override fun isSideLit(): Boolean {
        TODO("Not yet implemented")
    }

    override fun isBuiltin(): Boolean {
        TODO("Not yet implemented")
    }

    override fun getParticleSprite(): Sprite {
        TODO("Not yet implemented")
    }

    override fun getTransformation(): ModelTransformation {
        TODO("Not yet implemented")
    }

    override fun getOverrides(): ModelOverrideList {
        TODO("Not yet implemented")
    }

    override fun isVanillaAdapter(): Boolean {
        TODO("Not yet implemented")
    }

    override fun emitBlockQuads(
        blockView: BlockRenderView?,
        state: BlockState?,
        pos: BlockPos?,
        randomSupplier: Supplier<Random>?,
        context: RenderContext?
    ) {
        TODO("Not yet implemented")
    }

    override fun emitItemQuads(stack: ItemStack?, randomSupplier: Supplier<Random>?, context: RenderContext?) {
        TODO("Not yet implemented")
    }

}